package com.safebear.app;

import com.safebear.app.pages.*;
import com.safebear.app.utils.Utils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class BaseTest {
    WebDriver driver;
    Utils utility;
    WelcomePage welcomePage;
    LoginPage loginPage;
    UserPage userPage;
    FramesPage framesPage;
    FramesPageMainFrame framesPageMainFrame;
    ;

    @Before
    public void setUp() {
        utility = new Utils();
        driver = utility.getDriver();
        welcomePage = new WelcomePage(driver);
        loginPage = new LoginPage(driver);
        userPage = new UserPage(driver);
        framesPage = new FramesPage(driver);
        framesPageMainFrame = new FramesPageMainFrame(driver);
        driver.get(utility.getUrl());
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //   driver.manage().window().maximize();
    }

    @After
    public void tearDown() {
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }
}
